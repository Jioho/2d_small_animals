# 2D 小动物

## 在个人博客的地方添加一个会动的小动物 / 卡通人物！

效果图：
![](http://ww1.sinaimg.cn/large/98b4352agy1g294ehpozij205a05nglk.jpg)

## 比较复杂的流程

查看我的文章：[在自己项目或者 vue 中使用可爱的二次元（(Live2D）](https://blog.csdn.net/Jioho_chen/article/details/89419170)

## 有复杂的肯定也有简单的

> 本仓库已经编译了部分 JS。下面是如何直接在项目中使用这些小动物！

### 文件目录

```
├── live2dw
   ├── lib
   └── live2d-widget-model-chitose（这里还有很多相同文件夹，就不一一列出来了）
       └── assets
```

### 引入方法：

把下载的文件放在项目的资源目录
比如：`/public/live2dw`

在需要引用的页面中，引入 JS 资源

> index.html

```html
<script src="/live2dw/lib/L2Dwidget.min.js"></script>
```

然后是初始化方法。假设还在 index.html
**这里需要注意文件的加载顺序，很有可能`L2Dwidget.min.js`加载会比较慢**
> index.js

```js
 L2Dwidget.init({
   pluginRootPath: "live2dw/",
   pluginJsPath: "lib/",
   pluginModelPath: "live2d-widget-model-hijiki/assets/",
   tagMode: false,
   debug: false,
   model: { jsonPath: "/live2dw/live2d-widget-model-hijiki/assets/ hijiki.model.json" },
   display: { position: "right", width: 150, height: 300 },
   mobile: { show: true },
   log: false
 })
```

注意该配置的 `2,3,4,7` 行

| 配置名称 | 值 | 注意事项 |
|---------|-----|---------|
| pluginRootPath | 插件在项目的路径 | 注意这里的路径不要找错 |
| pluginJsPath | lib  (固定就是lib) | 这里就是接着上面的路径找下来的 `lib` 文件夹 |
| pluginModelPath | 这里是需要找到对要用的人物的 `assets` 路径 | 重点在于：`-hijik` 这里就是说找到 `hijik` 这个动画对应的资源文件 |
| model | 这里也是找 `assets` 路径的内容 | 注意路径不要弄错。这里也是从资源目录开始找 |

**打开你的项目，就发现已经多了一个小动物了~**

## 相关的链接和github地址
 - 已经整合了部分资源的项目 [https://github.com/stevenjoezhang/live2d-widget.git](https://github.com/stevenjoezhang/live2d-widget.git)

 - 项目资源: [https://github.com/xiazeyu/live2d-widget-models](https://github.com/xiazeyu/live2d-widget-models)

 - csdn 自己动手教程：[在自己项目或者 vue 中使用可爱的二次元（(Live2D）](https://blog.csdn.net/Jioho_chen/article/details/89419170)